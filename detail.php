<?php include("include/header.php"); ?>
<div class="wrapp-all listing-page detail-ad">
<section id="overall-wrp" class="listing-hero" style="    background: linear-gradient(#0000008a, #00000061, #00000038), url(images/detail/back1.jpg);
    height: 86px;">
   <div class=" bk-wrap-white">
     
   </div>
</section>
<!--- Our Video start---->
<section class="Featured_Products_wrap">
   <div class="container bk-wrap-white detail-page-profile overflow-unset">
      <div class="profile-wrap">
         <div class="cover-img">
            <img class="img-responsive" src="images/detail/clove_oil.png" style="width:100%">
         </div>
         <!--- profile slider start--->
         <section class="profile-img">
             <div class="profile-wraps-slider">
            <div class="col-md-3 col-md-offset-5">
               <div class="carousel slide" data-ride="carousel" id="quote-carousel">
                  <!-- Carousel Slides / Quotes -->
                  <div class="carousel-inner">
                     <!-- Quote 1 -->
                     <div class="item active">
                        <div class="row">
                           <div class="col-sm-12 text-center">
                              <img class="img-responsive" src="images/detail/box.jpg">
                           </div>
                        </div>
                     </div>
                     <div class="item">
                        <div class="row">
                           <div class="col-sm-12 text-center">
                              <img class="img-responsive" src="images/detail/box.jpg">
                           </div>
                        </div>
                     </div>
                  </div>
                  <!-- Carousel Buttons Next/Prev -->
                  <a data-slide="prev" href="#quote-carousel" class="left carousel-control"><i class="fa fa-long-arrow-left" aria-hidden="true"></i></a>
                  <a data-slide="next" href="#quote-carousel" class="right carousel-control"><i class="fa fa-long-arrow-right" aria-hidden="true"></i></a>
               </div>
            </div>
            </div>
         </section>
         <!---  profile sliders end --->
      </div>
   </div>
   <div class="container bk-wrap-white first-bk-img Featured_Products">
   <div class="Bottom-imgs padding-bottom-40">
   <div class="padding-mld">
      <div class="wrap-product-list">
      
                    <div class="about-wrp-oli first-section-Dlt">
                     <h2><span class="color-maroon">Clove Oil</span></h2>
                     <h4><span class="color-maroon">BOTANICAL NAME :</span> Lorem ipsum dolor sit amet</h4>
                     <a href="http://dev.glocalview.in/livinnature/index.php?route=common/home" target="_blank" class="buyNow"><img src="images/shop_now.svg" alt="" /></a><div class="clearfix"></div>
                     <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                     <div class="wrap-spice">
                        <div class="col-md-3">
                           <img src="images/detail/pro_c1.png" alt="" class="w-100">
                         
                        </div>
                        <div class="col-md-3">
                           <img src="images/detail/pro_c2.png" alt="" class="w-100">
                          
                        </div>
                        <div class="col-md-3">
                           <img src="images/detail/pro_c3.png" alt="" class="w-100">
                           
                        </div>
                        <div class="col-md-3">
                           <img src="images/detail/pro_c4.png" alt="" class="w-100">
                    
                        </div>
                     </div>
                     <div class="clearfix"></div>
                  </div>

                  <div class="about-wrp-oli bk-color">
                     <div class="text-center">
                     <img src="images/detail/aromatic.svg" alt="" style="margin-bottom:10px" />
                     </div>
                     <h2><span class="color-maroon">Aromatic </span> Description</h2>
                     <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. <br>consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                     <div class="wrap-spice">
                        <div class="col-md-2 col-md-offset-2">
                           <img src="images/detail/spice.png" alt="" class="w-100">
                           <h5>spice</h5>
                        </div>
                        <div class="col-md-2">
                           <img src="images/detail/woody.png" alt="" class="w-100">
                           <h5>woody</h5>
                        </div>
                        <div class="col-md-2">
                           <img src="images/detail/buds.png" alt="" class="w-100">
                           <h5>buds</h5>
                        </div>
                        <div class="col-md-2">
                           <img src="images/detail/bitter.png" alt="" class="w-100">
                           <h5>bitter</h5>
                        </div>
                     </div>
                     <div class="clearfix"></div>
                  </div>


                  <div class="about-wrp-oli bk-color">
                     <div class="succs">
                     <div class="text-center">
                     <img src="images/detail/process.svg" alt="" style="margin-bottom:10px; background:white" />
                     </div>
                     <h2><span class="color-maroon">Group </span> Cycle</h2>
                     
                     <img src="images/sess.png" alt="" />
                     <div class="clearfix"></div>
                     </div>
                  </div>



                  <div class="about-wrp-oli bk-color">
                     <div class="text-center">
                     <img src="images/detail/uses.svg" alt="" style="margin-bottom:10px" />
                     </div>
                     <h2><span class="color-maroon">Many uses of </span> Clove Oils</h2>
                     <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. <br>consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                     <div class="wrap-spice">
                        <div class="col-md-2">
                           <img src="images/detail/sweets.png" alt="" class="w-100">
                           <h5>sweets</h5>
                        </div>
                        <div class="col-md-2">
                           <img src="images/detail/cooking.png" alt="" class="w-100">
                           <h5>Cooking</h5>
                        </div>
                        <div class="col-md-2">
                           <img src="images/detail/drinks.png" alt="" class="w-100">
                           <h5>Drinks</h5>
                        </div>
                        <div class="col-md-2">
                           <img src="images/detail/arthritis.png" alt="" class="w-100">
                           <h5>Arthritis</h5>
                        </div>
                        <div class="col-md-2">
                           <img src="images/detail/asthma.png" alt="" class="w-100">
                           <h5>Asthma</h5>
                        </div>
                        <div class="col-md-2">
                           <img src="images/detail/toothache.png" alt="" class="w-100">
                           <h5>Toothache</h5>
                        </div>
                     </div>
                     <div class="clearfix"></div>
                  </div>

                  <div class="tables-dsgn">
                      <div class="title-tables">
                          <h3 class="text-uppercase">More Infomation about clove oil</h3>
                          <div class="wrap-rwo">
                              <div class="col-md-6 tabs-colm">
                              <img src="images/detail/color.svg" alt="" />
                                  <div>
                                  <h5>COLOR : </h5>
                                  <p>Golden Yellow/Brown</p>
                                  </div>
                              </div>

                              <div class="col-md-6 tabs-colm">
                                  <img src="images/detail/plant.svg" alt="" />
                                  
                                  <div>
                                  <h5>PLANT PART TYPICALLY USED : </h5>
                                  <p>Leaf & Bud</p>
                                  </div>
                              </div>

                          </div>

                          <div class="wrap-rwo">
                              <div class="col-md-6 tabs-colm">
                              <img src="images/detail/extraction.svg" alt="" />
                                  <div>
                                  <h5>EXTRACTION : </h5>
                                  <p>Steam Distilled </p>
                                  </div>
                              </div>

                              <div class="col-md-6 tabs-colm">
                              <img src="images/detail/odor.svg" alt="" />
                                  <div>
                                  <h5>STRENGTH OF INITIAL AROMA : </h5>
                                  <p>Strong</p>
                                  </div>
                              </div>

                          </div>

                      </div>
                  </div>


      </div>
      <!-- Produt list end -->
</section>
<!--- Our Video start---->
<section class="testimonial-section" id="ddls">
<div class="container bk-wrap-white Bottom-imgs">
<div class="padding-mld">
<div class="">
<span class="btun btn-color sub-btn">Similar Products</span>
<h2 class="title-holder"><span><strong class="color-maroon" >Viewed this</strong> item also</span></h2>
<div class="seprator"></div>
<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
<ol class="carousel-indicators">
<li data-target="#carousel" data-slide-to="0" class="active"></li>
<li data-target="#carousel" data-slide-to="1" class=""></li>
</ol>
<div class="controls">
<a class="left fa fa-chevron-left btn btn-default testimonial_btn" href="#carousel-example-generic"
   data-slide="prev"></a>
<a class="right fa fa-chevron-right btn btn-default testimonial_btn" href="#carousel-example-generic"
   data-slide="next"></a>
</div>
<!-- Wrapper for slides -->
<div class="carousel-inner">
<!-- item start -->
<div class="item active">
<div class="row">
<div class="col-md-4">
<img class="img-responsive" src="images/listing/3.jpg">
</div>
<div class="col-md-4">
<img class="img-responsive" src="images/listing/5.jpg">
</div>
<div class="col-md-4">
<img class="img-responsive" src="images/listing/2.jpg">
</div>
</div>
</div>
<!-- item end -->
<!-- item start -->
<div class="item ">
<div class="row">
<div class="col-md-4">
<img class="img-responsive" src="images/listing/7.jpg">
</div>
<div class="col-md-4">
<img class="img-responsive" src="images/listing/2.jpg">
</div>
<div class="col-md-4">
<img class="img-responsive" src="images/1.jpg">
</div>
</div>
</div>
<!-- item end -->
</div>
</div>
<a href="#" class="view-all-web">View All &nbsp; <i class="fa fa-long-arrow-right" aria-hidden="true"></i></a>
</div>
</div>
</div>
</section>
</div>
<?php include("include/footer.php"); ?>
