<?php include("include/header.php"); ?>
<div class="wrapp-all listing-page" id="listing">
      <section id="overall-wrp" class="listing-hero">
         <div class=" bk-wrap-white">
            <img src="images/Slider-217435180.jpg" alt="" class="w-100"/>
            </div>
      </section>
      <!--- Our Video start---->
      <section class="Featured_Products_wrap">
         <div class="container bk-wrap-white first-bk-img Featured_Products over-laping-Div">
            <div class="Bottom-imgs padding-bottom-40">
         <div class="padding-mld">
         <span class="btun btn-color sub-btn">Featured Products</span>
         <h2 class="title-holder"><span>Our <strong class="color-maroon">Featured</strong> Products</span></h2>
         
         
         <div class="wrap-product-list">
             <!--- listing div start -->
                <div class="listing">
                    <div class="row">
                            <div class="col-sm-5 text-center img-datas">
                              <img class="img-responsive" src="images/1.jpg">
                           </div>
                           <div class="col-sm-7 text-left">
                              <small><strong>Rosemary</strong> Oil</small>
                              <h4><strong class="color-maroon">MAIN COMPONENTS : </strong> Lorem Ipsum ist ein einfacher Demo-Text für die Print Lorem Ipsum ist ein einfacher Demo-Text für die Print</h4>
                              <p>Lorem Ipsum ist ein einfacher Demo-Text für die Print- und Schriftindustrie. Lorem Ipsum ist in der Industrie bereits der Standard Demo-Text Lorem Ipsum ist ein einfacher Demo-Text für die Print- und Schriftindustrie. Lorem Ipsum ist in der Industrie bereits der Standard Demo-Text. <a href="detail.php">Read More</a></p>
                           </div>
                           
                    </div>
                    </div>
                <!--- listing div end -->
                <!--- listing div start -->
                <div class="listing single-left-img">
                    <div class="row ">
                           <div class="col-sm-7 text-left">
                              <small><strong>Ajwain</strong> Oil</small>
                              <h4><strong class="color-maroon">MAIN COMPONENTS : </strong> Lorem Ipsum ist ein einfacher Demo-Text für die Print</h4>
                              <p>Lorem Ipsum ist ein einfacher Demo-Text für die Print- und Schriftindustrie. Lorem Ipsum ist in der Industrie bereits der Standard Demo-Text.Lorem Ipsum ist in der Industrie bereits der Standard Demo-Text.Lorem Ipsum ist in der Industrie bereits der Standard Demo-Text. <a href="detail.php">Read More</a></p>
                           </div>
                           <div class="col-sm-5 text-center img-datas">
                              <img class="img-responsive" src="images/listing/2.jpg">
                           </div>
                    </div>
                    </div>
                <!--- listing div end -->


                <!--- listing div start -->
                <div class="listing ">
                    <div class="row">
                            <div class="col-sm-5 text-center img-datas">
                              <img class="img-responsive" src="images/listing/3.jpg">
                           </div>
                           <div class="col-sm-7 text-left">
                              <small><strong>Lavender</strong> Oil</small>
                              <h4><strong class="color-maroon">MAIN COMPONENTS : </strong> Lorem Ipsum ist ein einfacher Demo-Text für die Print Lorem Ipsum ist ein einfacher Demo-Text für die Print</h4>
                              <p>Lorem Ipsum ist ein einfacher Demo-Text für die Print- und Schriftindustrie. Lorem Ipsum ist in der Industrie bereits der Standard Demo-Text Lorem Ipsum ist ein einfacher Demo-Text für die Print- und Schriftindustrie. Lorem Ipsum ist in der Industrie bereits der Standard Demo-Text. <a href="detail.php">Read More</a></p>
                           </div>
                           
                    </div>
                    </div>
                <!--- listing div end -->
                <!--- listing div start -->
                <div class="listing">
                    <div class="row">
                           <div class="col-sm-7 text-left">
                              <small><strong>Cintronella</strong> Oil</small>
                              <h4><strong class="color-maroon">MAIN COMPONENTS : </strong> Lorem Ipsum ist ein einfacher Demo-Text für die Print</h4>
                              <p>Lorem Ipsum ist ein einfacher Demo-Text für die Print- und Schriftindustrie. Lorem Ipsum ist in der Industrie bereits der Standard Demo-Text.Lorem Ipsum ist in der Industrie bereits der Standard Demo-Text.Lorem Ipsum ist in der Industrie bereits der Standard Demo-Text.</p>
                           </div>
                           <div class="col-sm-5 text-center img-datas">
                              <img class="img-responsive" src="images/listing/5.jpg">
                           </div>
                    </div>
                    </div>
                <!--- listing div end -->

                    
            </div>

         <!-- Produt list end -->
         
         
         
         
         
         
         
         
      </section>
     
    
     
      <!--- Our Video start---->
      <section class="testimonial-section smilisr-pro" id="ddls">
         <div class="container bk-wrap-white Bottom-imgs">
            <div class="padding-mld">
               <div class="">
                  <span class="btun btn-color sub-btn">Related Products</span>
                  <h2 class="title-holder"><span><strong class="color-maroon" >Related</strong> Products</span></h2>
                  <div class="seprator"></div>
                  <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                  <ol class="carousel-indicators">
                     <li data-target="#carousel" data-slide-to="0" class="active"></li>
                     <li data-target="#carousel" data-slide-to="1" class=""></li>
                     
                  </ol>
                     <div class="controls">
                        <a class="left fa fa-chevron-left btn btn-default testimonial_btn" href="#carousel-example-generic"
                           data-slide="prev"></a>
                        <a class="right fa fa-chevron-right btn btn-default testimonial_btn" href="#carousel-example-generic"
                           data-slide="next"></a>
                     </div>
                     <!-- Wrapper for slides -->
                     <div class="carousel-inner">

                        <!-- item start -->
                        <div class="item active">
                           <div class="row">
                               <div class="col-md-4">
                                    <img class="img-responsive" src="images/listing/3.jpg">
                               </div>
                               <div class="col-md-4">
                                    <img class="img-responsive" src="images/listing/5.jpg">
                               </div>
                               <div class="col-md-4">
                                    <img class="img-responsive" src="images/listing/2.jpg">
                               </div>
                           </div>
                        </div>
                        <!-- item end -->

                        <!-- item start -->
                        <div class="item ">
                           <div class="row">
                               <div class="col-md-4">
                                    <img class="img-responsive" src="images/listing/7.jpg">
                               </div>
                               <div class="col-md-4">
                                    <img class="img-responsive" src="images/listing/2.jpg">
                               </div>
                               <div class="col-md-4">
                                    <img class="img-responsive" src="images/1.jpg">
                               </div>
                           </div>
                        </div>
                        <!-- item end -->

                        
                     </div>
                  </div>
                  <a href="#" class="view-all-web">View All &nbsp; <i class="fa fa-long-arrow-right" aria-hidden="true"></i></a>
               </div>
            </div>
         </div>
      </section>
      </div>
      <?php include("include/footer.php"); ?>