<?php include("include/header.php"); ?>
<div class="wrapp-all listing-page" id="listing">
      <section id="overall-wrp" class="listing-hero">
         <div class=" bk-wrap-white">
            <img src="images/upcoming/upcoming-banner.png" alt="" class="w-100"/>
            </div>
      </section>
      <!--- Our Video start---->
      <section class="Featured_Products_wrap upcoming-page">
         <div class="container bk-wrap-white first-bk-img Featured_Products over-laping-Div">
            <div class="Bottom-imgs padding-bottom-40">
         <div class="padding-mld">
         <span class="btun btn-color sub-btn">Upcoming Products</span>
         <h2 class="title-holder"><span><strong class="color-maroon">Upcoming</strong> Products</span></h2>
         
         
         <div class="wrap-product-list">
             <!--- listing div start -->
                <div class="listing">
                    <div class="row">
                            <div class="col-sm-5 text-center img-datas">
                              <img class="img-responsive" src="images/upcoming/a1.png">
                           </div>
                           <div class="col-sm-7 text-left">
                              <small><strong>Kesar Rose</strong> Drops</small>
                              <p>Kesar Rose Drops are pure & natural extracts that retain all the natural goodness and authentic taste of Kesar & Rose along with all of their health benefits like: Aids weight loss, treats insomnia, Anti depressing, treats migraine, hormone healer, etc.</p>
                              <h3>RAW MATERIAL USED :</h3>
                                <p> Our sustainable business model is founded on long term relationships with local farmers to provide best quality natural products.</p>
                                <ol>
                                    <li> Comes in 5 ml bottle , 150 drops Approx.</li>
                                    <li> It’s fast and easy to use and a lot more affordable.</li>
                                </ol>
                           </div>
                           
                    </div>
                    </div>
                <!--- listing div end -->
                <!--- listing div start -->
                <div class="listing single-left-img">
                    <div class="row ">
                           <div class="col-sm-7 text-left">
                              <small><strong>Lemon </strong> Drops</small>
                                <p>Lemon Drops are pure & natural extracts that retain all the natural goodness and authentic taste of Lemon along with all its health benefits like: Soothes respiratory disorders, reduces corns, helps in skin brightening, alleviates toothache, soothes inflammation, etc.</p>   

                                <h3>RAW MATERIAL USED :</h3>
                                <p> Our sustainable business model is founded on long term relationships with local farmers to provide best quality natural products.</p>
                                <ol>
                                    <li> Comes in 5 ml bottle , 150 drops Approx.</li>
                                    <li> 2. It’s fast and easy to use and a lot more affordable.</li>
                                </ol>

                            </div>
                           <div class="col-sm-5 text-center img-datas">
                              <img class="img-responsive" src="images/upcoming/a2.png">
                           </div>
                    </div>
                    </div>
                <!--- listing div end -->


                <!--- listing div start -->
                <div class="listing ">
                    <div class="row">
                            <div class="col-sm-5 text-center img-datas">
                              <img class="img-responsive" src="images/upcoming/a3.png">
                           </div>
                           <div class="col-sm-7 text-left">
                              <small><strong>Lime</strong> Drops</small>
                              <h4><strong class="color-maroon">MAIN COMPONENTS : </strong> Lorem Ipsum ist ein einfacher Demo-Text für die Print Lorem Ipsum ist ein einfacher Demo-Text für die Print</h4>
                              <p>Lorem Ipsum ist ein einfacher Demo-Text für die Print- und Schriftindustrie. Lorem Ipsum ist in der Industrie bereits der Standard Demo-Text Lorem Ipsum ist ein einfacher Demo-Text für die Print- und Schriftindustrie. Lorem Ipsum ist in der Industrie bereits der Standard Demo-Text. <a href="detail.php">Read More</a></p>
                           </div>
                           
                    </div>
                    </div>
                <!--- listing div end -->
                <!--- listing div start -->
                <div class="listing">
                    <div class="row">
                           <div class="col-sm-7 text-left">
                              <small><strong>Mint </strong> Drops</small>
                              <h4><strong class="color-maroon">MAIN COMPONENTS : </strong> Lorem Ipsum ist ein einfacher Demo-Text für die Print</h4>
                              <p>Lorem Ipsum ist ein einfacher Demo-Text für die Print- und Schriftindustrie. Lorem Ipsum ist in der Industrie bereits der Standard Demo-Text.Lorem Ipsum ist in der Industrie bereits der Standard Demo-Text.Lorem Ipsum ist in der Industrie bereits der Standard Demo-Text.</p>
                           </div>
                           <div class="col-sm-5 text-center img-datas">
                              <img class="img-responsive" src="images/upcoming/a4.png">
                           </div>
                    </div>
                    </div>
                <!--- listing div end -->

                    
            </div>

         <!-- Produt list end -->
         
         
         
         
         
         
         
         
      </section>
     
    
     
      <!--- Our Video start---->
      <section class="testimonial-section smilisr-pro" id="ddls">
         <div class="container bk-wrap-white Bottom-imgs">
            <div class="padding-mld">
               <div class="">
                  <span class="btun btn-color sub-btn">Smiliar Products</span>
                  <h2 class="title-holder upcoming-pro"><span><strong class="color-maroon" >Related</strong> Products</span></h2>
                  <div class="seprator"></div>
                  <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                  <ol class="carousel-indicators">
                     <li data-target="#carousel" data-slide-to="0" class="active"></li>
                     <li data-target="#carousel" data-slide-to="1" class=""></li>
                     
                  </ol>
                     <div class="controls">
                        <a class="left fa fa-chevron-left btn btn-default testimonial_btn" href="#carousel-example-generic"
                           data-slide="prev"></a>
                        <a class="right fa fa-chevron-right btn btn-default testimonial_btn" href="#carousel-example-generic"
                           data-slide="next"></a>
                     </div>
                     <!-- Wrapper for slides -->
                     <div class="carousel-inner">

                        <!-- item start -->
                        <div class="item active">
                           <div class="row">
                               <div class="col-md-4">
                                    <img class="img-responsive" src="images/listing/3.jpg">
                               </div>
                               <div class="col-md-4">
                                    <img class="img-responsive" src="images/listing/5.jpg">
                               </div>
                               <div class="col-md-4">
                                    <img class="img-responsive" src="images/listing/2.jpg">
                               </div>
                           </div>
                        </div>
                        <!-- item end -->

                        <!-- item start -->
                        <div class="item ">
                           <div class="row">
                               <div class="col-md-4">
                                    <img class="img-responsive" src="images/listing/7.jpg">
                               </div>
                               <div class="col-md-4">
                                    <img class="img-responsive" src="images/listing/2.jpg">
                               </div>
                               <div class="col-md-4">
                                    <img class="img-responsive" src="images/1.jpg">
                               </div>
                           </div>
                        </div>
                        <!-- item end -->

                        
                     </div>
                  </div>
                  <a href="#" class="view-all-web">View All &nbsp; <i class="fa fa-long-arrow-right" aria-hidden="true"></i></a>
               </div>
            </div>
         </div>
      </section>
      </div>
      <?php include("include/footer.php"); ?>