<?php include("include/header.php"); ?>
<div class="wrapp-all listing-page" id="listing">
   <section id="overall-wrp" class="listing-hero">
      <div class=" bk-wrap-white">
         <img src="images/jpeg/csr_banner.png" alt="" class="w-100"/>
      </div>
   </section>
   <section class="over-laping-Div">
      <div class="container bk-wrap-white  wrps-about">
         <div class="padding-mld">
            <div class="about-page-2 text-left">
               <h1>Corporate Social Responsibility</h1>
                
                
               <div class="col-md-6 text-left">
               <p>Our action today, define the future generation, hence 'How we act now will decide the world we provide to our younger generation'.</p>
                  <div class="qouets text-left" style="padding:0">
                   
                     <h2> We believe that every successful business must have a social impact.</h2>
                    
                  </div>

                   

                  <p>VDH Group has been associated with Sikhtal Charitable Dispensary where patient can consult doctors free of charge and provided free medicine, minor operation such as eye surgery etc. are conducted free of charge.</p>
                  <p>We are also associated with Lakshyam, a non-governmental organization, established for the welfare of the society, and Engaged in a number of social welfare activities like child welfare, health, education and women empowerment  </p>

     
               </div>
               <div class="col-md-6 text-right">
                   
                  <img src="images/jpeg/csr_image.png" alt="" class="w100" />
               </div>


            <div class="clearfix"></div>
               
           <div class="csr-galler">
               <h2>&nbsp;&nbsp;<span class="color-maroon">CSR Gallery</span></h2>
               <div class="col-md-4">
                   <div class="wrap-gall">
                   <img src="images/jpeg/csr_1.png" alt="" />
                   </div>
               </div>

               <div class="col-md-4">
                   <div class="wrap-gall">
                   <img src="images/jpeg/csr_2.png" alt="" />
                   </div>
               </div>

               <div class="col-md-4">
                   <div class="wrap-gall">
                   <img src="images/jpeg/csr_3.png" alt="" />
                   </div>
               </div>


               <!-- <div class="col-md-4">
                   <div class="wrap-gall">
                   <img src="images/jpeg/csr_4.png" alt="" />
                   </div>
               </div> -->

               <div class="col-md-4">
                   <div class="wrap-gall">
                   <img src="images/jpeg/csr_5.png" alt="" />
                   </div>
               </div>

               <div class="col-md-4">
                   <div class="wrap-gall">
                   <img src="images/jpeg/csr_6.png" alt="" />
                   </div>
               </div>

               <div class="col-md-4">
                   <div class="wrap-gall">
                   <img src="images/jpeg/csr_7.png" alt="" />
                   </div>
               </div>

               <div class="col-md-4">
                   <div class="wrap-gall">
                   <img src="images/jpeg/csr_8.png" alt="" />
                   </div>
               </div>

                
           </div>


                 
            </div>
         </div>
      </div>
   </section>
   
    
</div>
<br>
<?php include("include/footer.php"); ?>