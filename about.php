<?php include("include/header.php"); ?>
<div class="wrapp-all listing-page" id="listing">
   <section id="overall-wrp" class="listing-hero">
      <div class=" bk-wrap-white">
         <img src="images/Slider-217435180.jpg" alt="" class="w-100"/>
      </div>
   </section>
   <section class="over-laping-Div">
      <div class="container bk-wrap-white  wrps-about">

         <div class="padding-mld">
            <div class="about-page-2 text-left">
               <h1>About Us</h1>
               <h3>How it all began...</h3>
               <div class="col-md-6 text-left">
                  <p>VDH is a family owned group of companies in India, started with a humble beginning 40yrs ago in 1976 with a retail chain business foundation laid by the committed & strong willed Mr. Paramjeet Singh, our Chairman. It started with its first manufacturing unit of aromatic chemicals by our founder & visionary Mr. Guneet Singh in 1996. Through years of hard work and dedication, by 2003 the company became the first in the nation to start the production of thymol from Meta cresols. Along the years, the board of company was joined by the young & dynamic brothers Mr. Ramandeep Singh, Mr. Chanpreet Singh & Mr. Gagandeep Singh. Their joint efforts committed to create a win-win situation for both the customers and the company, made VDH the first and only firm in India to start the production of Meta Cresols through backward integration and also the production of Para Cresol, BHT along with other antioxidants. In 2010 giving a new vision to the company together The VDH Brothers started a new manufacturing facility for producing all Natural products with a wide range of essential oils and spice oils. Developing through years along with the ever growing customer base, company started its own warehousing facility in Europe (Germany) to provide faster deliveries to its foreign clientele.  By 2017 with her confident persona Ms. Ashmeen Kaur, Daughter of Mr. Guneet Singh entered the business, introducing the 3RD generation of the family to the company.</p>
               </div>
               <div class="col-md-6 text-left">
                  <p>  All these years the company’s R&D team has played a vital role in its growth while innovatively making its consumer’s lives easier. With the same thought, The Company came up with a futuristic approach towards the spice industry by developing a revolutionary product to change dynamics of the industry as well as the common households. The company launched its new brand <span class="color-maroon" > LIV IN NATURE</span> with all natural range of spices in liquid form to make the process of cooking much more convenient, faster and easier while providing all the crucial nutrients your body needs. </p>
                  <img src="images/about/ceo.png" alt="" class="w100" />
               </div>
               <div class="col-md-12">
                  <div class="qouets text-left">
                   <h2>As the world leader in essential oils, we believe that everyone deserves a life full of abundance and wellness. Our vision is to bring the life-changing benefits of essential oils to every home, family, and lifestyle.</h2>
                   <p>As the world leader in essential oils, we believe that everyone deserves a life full of abundance and wellness. Our vision is to bring the life-changing benefits of essential oils to every home, family, and lifestyle.</p>
                  </div>
               </div>
               <div class="clearfix"></div>
               <!---design pattern start -->
               <section>
                  <div class=" pros-wraps">
                     <h5 style="    font-size: 26px;
                        font-weight: 600;
                        color: #923233;
                        margin-top: 47px;">Our Every Step - History</h5>

<section id="timeline">
  <ul>
    <li>
      <div class="tilks">
        <h3>
        1976
        </h3>
        <p>Formation of VDH Group</p>
      </div>
    </li>

    <li>
      <div>
        <h3>
           1996 - 97
        </h3>
        <p>Started production of Thymol from Menthone</p>
      </div>
    </li>

    <li>
      <div>
        <h3>
           2003
        </h3>
        <p>First in India to start production of thymol from Meta Cresol</p>
      </div>
    </li>

    <li>
      <div>
        <h3>
          2007
        </h3>
        <p>First (and only) manufacturer in India to start production of Meta Cresol (Backward integration) started producing ParaCresol,BHT</p>
      </div>
    </li>

    <li>
      <div>
        <h3>
           2010
        </h3>
        <p>Started new manufacturing facility for production of 100% Natural essential & Spice Oil</p>
      </div>
    </li>

    <li>
      <div>
        <h3>
           2012
        </h3>
        <p>Started Warehousing facility in Europe (Germany)</p>
      </div>
    </li>

    <li>
      <div>
        <h3>
         2014
        </h3>
        <p>New Derivatives from Thymol such as Synthetic Menthones & Synthetic DL-Menthol</p>
      </div>
    </li>

    <li>
      <div>
        <h3>
          2015
        </h3>
        <p>Largest manufacturer of (Butylated Hydroxytoluene) BHT in India. New Derivatives of Meta Cresol & Para Cresol such as 2-tert-butyl-5-methylphenol, 2-tert-butyl-4-methylphenol & Chloro Cresol</p>
      </div>
    </li>

  

  </ul>
</section>

                 </div>
               </section>
               <!---- desgin pattern end -->
            </div>
         </div>
      </div>
   </section>

 
<section id="teams-page">
      <div class="container bk-wrap-white  wrps-about">
         <div class="padding-mld">
           

            <!-- START TEAM -->
    <section id="team">
        <div>
            <div class="row">
                <div class="col-md-5 col-sm-12 pull-right">
                    <div class="team-section-text">
                        <div class="section-count">
                            <span>Our Pillars</span>
                        </div>
                        <!-- END section-count-->
                        <div class="section-text">
                            <h2 class="section-title">Live In Nature <br> Team</h2>
                            <p>
                            Our Key Team Members.
                            </p>
                        </div>
                        <!-- END section-text-->
                    </div>
                    <!-- END team-section-text-->
                </div>
                <!-- END col-md-5 col-sm-12 pull-right-->
                <div class="col-md-7 col-sm-12">
                    <div class="row">
                        <div class="col-md-3 col-sm-4">
                            <div class="team-list">
                                <ul>
                                    <li class="wow zoomIn" data-wow-duration="1s" data-wow-delay=".1s">
                                        <a href="#team-1" data-team="team-1">
                                            <figure>
                                                <img src="images/yy.png" alt="Team Member image One">
                                            </figure>
                                        </a>
                                    </li>
                                    <li class="active wow zoomIn" data-wow-duration="1s" data-wow-delay=".3s">
                                        <a href="#team-2" data-team="team-2">
                                            <figure>
                                                <img src="images/yy.png" alt="Team Member image two">
                                            </figure>
                                        </a>
                                    </li>
                                    <li class="wow zoomIn" data-wow-duration="1s" data-wow-delay=".5s">
                                        <a href="#team-3" data-team="team-3">
                                            <figure>
                                                <img src="images/yy.png" alt="Team Member image three">
                                            </figure>
                                        </a>
                                    </li>
                                    <li class="wow zoomIn" data-wow-duration="1s" data-wow-delay=".7s">
                                        <a href="#team-4" data-team="team-4">
                                            <figure>
                                                <img src="images/yy.png" alt="Team Member image four">
                                            </figure>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                            <!-- END team-list-->
                        </div>
                        <!-- END col-sm-4-->
                        <div class="col-md-9 col-sm-8">

                            <div id="team-1" class="team-single">
                                <div class="team-img">
                                    <img src="images/yy.png" alt="">
                                    <div class="team-social">
                                        <ul>
                                            <li><a href=""><i class="fa fa-facebook"></i></a></li>
                                            <li><a href=""><i class="fa fa-twitter"></i></a></li>
                                            <li><a href=""><i class="fa fa-google-plus"></i></a></li>
                                            <li><a href=""><i class="fa fa-linkedin"></i></a></li>
                                        </ul>
                                    </div>
                                    <!-- END team-social-->
                                </div>
                                <!-- END team-img-->
                                <div class="team-info text-center">
                                    <h4>Guneet Singh</h4>
                                    <p>Managing Director</p>
                                    <p>The founder and visionary started this company with a small dream while having a broad vision in mind. With the blessings of God & his parent’s guidance, he has come a long way in the journey of success using his charisma & influential personality to move forward towards the pathway of achieving goals. His story is an inspiration for young entrepreneurs to dream big!!</p>
                                </div>
                                <!-- END team-info-->
                            </div>
                            <!-- END team-single-->


                            <div id="team-2" class="team-single active">
                                <div class="team-img">
                                    <img src="images/yy.png" alt="">
                                    <div class="team-social">
                                        <ul>
                                            <li><a href=""><i class="fa fa-facebook"></i></a></li>
                                            <li><a href=""><i class="fa fa-twitter"></i></a></li>
                                            <li><a href=""><i class="fa fa-google-plus"></i></a></li>
                                            <li><a href=""><i class="fa fa-linkedin"></i></a></li>
                                        </ul>
                                    </div>
                                    <!-- END team-social-->
                                </div>
                                <!-- END team-img-->
                                <div class="team-info text-center">
                                    <h4>Ramandeep Singh</h4>
                                    <p>Director</p>
                                    <p>The young at heart with a Magnetic personality, heading the sales & marketing department for both the import and export of the company is intensely focused having his eyes set towards the goal. His energy & optimism is infectious throughout the organization and his open-mindedness allows navigating stressful situations with a flexible mind.</p>
                                </div>
                                <!-- END team-info-->
                            </div>
                            <!-- END team-single-->


                            <div id="team-3" class="team-single">
                                <div class="team-img">
                                    <img src="images/yy.png" alt="">
                                    <div class="team-social">
                                        <ul>
                                            <li><a href=""><i class="fa fa-facebook"></i></a></li>
                                            <li><a href=""><i class="fa fa-twitter"></i></a></li>
                                            <li><a href=""><i class="fa fa-google-plus"></i></a></li>
                                            <li><a href=""><i class="fa fa-linkedin"></i></a></li>
                                        </ul>
                                    </div>
                                    <!-- END team-social-->
                                </div>
                                <!-- END team-img-->
                                <div class="team-info text-center">
                                    <h4>Gagandeep Singh</h4>
                                    <p>Director</p>
                                    <p>Gem of a personality & very influential, contributing his expertise towards new innovations. He has a compelling vision towards the industry introducing revolutionary products to make the lives of the consumers easy. Being courageous and daring, he is willing to take calculated risks solely for the growth & development of the company.</p>
                                </div>
                                <!-- END team-info-->
                            </div>
                            <!-- END team-single-->


                            <div id="team-4" class="team-single">
                                <div class="team-img">
                                    <img src="images/yy.png" alt="">
                                    <div class="team-social">
                                        <ul>
                                            <li><a href=""><i class="fa fa-facebook"></i></a></li>
                                            <li><a href=""><i class="fa fa-twitter"></i></a></li>
                                            <li><a href=""><i class="fa fa-google-plus"></i></a></li>
                                            <li><a href=""><i class="fa fa-linkedin"></i></a></li>
                                        </ul>
                                    </div>
                                    <!-- END team-social-->
                                </div>
                                <!-- END team-img-->
                                <div class="team-info text-center">
                                    <h4>Chanpreet Singh</h4>
                                    <p>Director</p>
                                    <p>The exceptionally brainy with a futuristic sensibility, he heads the technical & production department and never hesitates to go an extra mile while keeping a stringent check to deliver the highest quality of products for the beloved consumers. Understanding the importance of time of oneself & others, being a very punctual & hard working member on board; he never fails to inspire others around him to strive towards improvement.</p>
                                </div>
                                <!-- END team-info-->
                            </div>
                            <!-- END team-single-->

                        </div>
                        <!-- END col-sm-8-->
                    </div>
                    <!-- END row-->
                </div>
                <!-- END col-md-7 col-sm-12 -->
            </div>
            <!-- END row-->
        </div>
        <!-- END container-->
    </section>
    <!-- END team-->
    <!-- END TEAM -->
 

         </div>
      </div>
   </section> 

 

   <section>
      <div class="container wrps-about">
         <div class="padding-mld" style="padding:50px">
         <h3><span class="color-maroon" style="font-size:28px;">Our Logo</span></h3>
            <div class="text-left succs logo-mean" style="padding:17px 0">
               
               <div class="col-md-9">
                  
                  <p style="margin-top: 40px;">To bring the goodness of nature back in our lives and LIV IN the harmony of nature and allowing it to as nature provides us nourishment and protection from all the hazardous chemicals that are slowly and steadily harming our body in tones of ways.</p>
               </div>
               <div class="col-md-3">
                  <img src="images/logo-liv.png" alt="" style="max-width:100%">
               </div>
               <div class="clearfix"></div>
            </div>
            <div class="row standfor">
               <h4>What We Stand For</h4>
               <div class="col-md-6 text-left" style="border-right: 1px solid #ccccccab;">
                  <img src="images/about/vision.svg" alt="" />
                  <h5>VISION</h5>
                <p>Through credibility & innovation, our ambition is to change the scenario of the health industry in the coming future by providing ready to use ALL NATURAL products to our consumers so that they can lead a healthy lifestyle that too with an ease while making a positive contribution to our community’s health & wellbeing. By our experience, in order to make a bigger impact, we make sure to set new goals for the company to grow as a market leader while not compromising our ethics on any stage of development. As our Values have always been pillar to our success.</p>
               </div>
               <div class="col-md-6 text-left">
                  <img src="images/about/mission.svg" alt="" />
                  <h5>MISSION - Time for change! </h5>
                 <p>To start a movement of rebel using the wisdom of nature against the harmful n dangerous world of chemicals that we are trapped in as they are slowly and steadily harming our body from inside out. Our Main Aim is to restore the benefits of nature in our lives while adapting it to our lifestyles. As we all use chemical products just because they are easy to use but now our mission is to bring the goodness of nature to our consumers with simple and easy, ready to use products that take no time and efforts.</p>
               </div>
            </div>
         </div>
      </div>
   </section>
</div>
<br>
<?php include("include/footer.php"); ?>