<?php include("include/header.php"); ?>

<div class="wrapp-all listing-page" id="teams-page">
   <section id="overall-wrp" class="listing-hero">
      <div class=" bk-wrap-white">
         <img src="images/jpeg/caution_banner.png" alt="" class="w-100"/>
      </div>
   </section>




   <section class="over-laping-Div">
      <div class="container bk-wrap-white  wrps-about">
         <div class="padding-mld">
           

            <!-- START TEAM -->
    <section id="team">
        <div>
            <div class="row">
                <div class="col-md-5 col-sm-12 pull-right">
                    <div class="team-section-text">
                        <div class="section-count">
                            <span>Our Pillars</span>
                        </div>
                        <!-- END section-count-->
                        <div class="section-text">
                            <h2 class="section-title">Live In Nature <br> Team</h2>
                            <p>
                            Our Key Team Members.
                            </p>
                        </div>
                        <!-- END section-text-->
                    </div>
                    <!-- END team-section-text-->
                </div>
                <!-- END col-md-5 col-sm-12 pull-right-->
                <div class="col-md-7 col-sm-12">
                    <div class="row">
                        <div class="col-md-3 col-sm-4">
                            <div class="team-list">
                                <ul>
                                    <li class="wow zoomIn" data-wow-duration="1s" data-wow-delay=".1s">
                                        <a href="#team-1" data-team="team-1">
                                            <figure>
                                                <img src="images/yy.png" alt="Team Member image One">
                                            </figure>
                                        </a>
                                    </li>
                                    <li class="active wow zoomIn" data-wow-duration="1s" data-wow-delay=".3s">
                                        <a href="#team-2" data-team="team-2">
                                            <figure>
                                                <img src="images/yy.png" alt="Team Member image two">
                                            </figure>
                                        </a>
                                    </li>
                                    <li class="wow zoomIn" data-wow-duration="1s" data-wow-delay=".5s">
                                        <a href="#team-3" data-team="team-3">
                                            <figure>
                                                <img src="images/yy.png" alt="Team Member image three">
                                            </figure>
                                        </a>
                                    </li>
                                    <li class="wow zoomIn" data-wow-duration="1s" data-wow-delay=".7s">
                                        <a href="#team-4" data-team="team-4">
                                            <figure>
                                                <img src="images/yy.png" alt="Team Member image four">
                                            </figure>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                            <!-- END team-list-->
                        </div>
                        <!-- END col-sm-4-->
                        <div class="col-md-9 col-sm-8">

                            <div id="team-1" class="team-single">
                                <div class="team-img">
                                    <img src="images/yy.png" alt="">
                                    <div class="team-social">
                                        <ul>
                                            <li><a href=""><i class="fa fa-facebook"></i></a></li>
                                            <li><a href=""><i class="fa fa-twitter"></i></a></li>
                                            <li><a href=""><i class="fa fa-google-plus"></i></a></li>
                                            <li><a href=""><i class="fa fa-linkedin"></i></a></li>
                                        </ul>
                                    </div>
                                    <!-- END team-social-->
                                </div>
                                <!-- END team-img-->
                                <div class="team-info text-center">
                                    <h4>Guneet Singh</h4>
                                    <p>Managing Director</p>
                                    <p>The founder and visionary started this company with a small dream while having a broad vision in mind. With the blessings of God & his parent’s guidance, he has come a long way in the journey of success using his charisma & influential personality to move forward towards the pathway of achieving goals. His story is an inspiration for young entrepreneurs to dream big!!</p>
                                </div>
                                <!-- END team-info-->
                            </div>
                            <!-- END team-single-->


                            <div id="team-2" class="team-single active">
                                <div class="team-img">
                                    <img src="images/yy.png" alt="">
                                    <div class="team-social">
                                        <ul>
                                            <li><a href=""><i class="fa fa-facebook"></i></a></li>
                                            <li><a href=""><i class="fa fa-twitter"></i></a></li>
                                            <li><a href=""><i class="fa fa-google-plus"></i></a></li>
                                            <li><a href=""><i class="fa fa-linkedin"></i></a></li>
                                        </ul>
                                    </div>
                                    <!-- END team-social-->
                                </div>
                                <!-- END team-img-->
                                <div class="team-info text-center">
                                    <h4>Ramandeep Singh</h4>
                                    <p>Director</p>
                                    <p>The young at heart with a Magnetic personality, heading the sales & marketing department for both the import and export of the company is intensely focused having his eyes set towards the goal. His energy & optimism is infectious throughout the organization and his open-mindedness allows navigating stressful situations with a flexible mind.</p>
                                </div>
                                <!-- END team-info-->
                            </div>
                            <!-- END team-single-->


                            <div id="team-3" class="team-single">
                                <div class="team-img">
                                    <img src="images/yy.png" alt="">
                                    <div class="team-social">
                                        <ul>
                                            <li><a href=""><i class="fa fa-facebook"></i></a></li>
                                            <li><a href=""><i class="fa fa-twitter"></i></a></li>
                                            <li><a href=""><i class="fa fa-google-plus"></i></a></li>
                                            <li><a href=""><i class="fa fa-linkedin"></i></a></li>
                                        </ul>
                                    </div>
                                    <!-- END team-social-->
                                </div>
                                <!-- END team-img-->
                                <div class="team-info text-center">
                                    <h4>Gagandeep Singh</h4>
                                    <p>Director</p>
                                    <p>Gem of a personality & very influential, contributing his expertise towards new innovations. He has a compelling vision towards the industry introducing revolutionary products to make the lives of the consumers easy. Being courageous and daring, he is willing to take calculated risks solely for the growth & development of the company.</p>
                                </div>
                                <!-- END team-info-->
                            </div>
                            <!-- END team-single-->


                            <div id="team-4" class="team-single">
                                <div class="team-img">
                                    <img src="images/yy.png" alt="">
                                    <div class="team-social">
                                        <ul>
                                            <li><a href=""><i class="fa fa-facebook"></i></a></li>
                                            <li><a href=""><i class="fa fa-twitter"></i></a></li>
                                            <li><a href=""><i class="fa fa-google-plus"></i></a></li>
                                            <li><a href=""><i class="fa fa-linkedin"></i></a></li>
                                        </ul>
                                    </div>
                                    <!-- END team-social-->
                                </div>
                                <!-- END team-img-->
                                <div class="team-info text-center">
                                    <h4>Chanpreet Singh</h4>
                                    <p>Director</p>
                                    <p>The exceptionally brainy with a futuristic sensibility, he heads the technical & production department and never hesitates to go an extra mile while keeping a stringent check to deliver the highest quality of products for the beloved consumers. Understanding the importance of time of oneself & others, being a very punctual & hard working member on board; he never fails to inspire others around him to strive towards improvement.</p>
                                </div>
                                <!-- END team-info-->
                            </div>
                            <!-- END team-single-->

                        </div>
                        <!-- END col-sm-8-->
                    </div>
                    <!-- END row-->
                </div>
                <!-- END col-md-7 col-sm-12 -->
            </div>
            <!-- END row-->
        </div>
        <!-- END container-->
    </section>
    <!-- END team-->
    <!-- END TEAM -->





         </div>
      </div>
   </section>
   
</div>
<br>


<style>
 
 
#main-header {
  padding: 4rem 0;
  background: var(--secondary-color);
}
 


/* Create Line */
#timeline ul {
  padding: 50x 0;
  background: var(--primary-color);
}

#timeline ul li {
  position: relative;
  width: 1px;
  padding-top: 50px;
  margin: 0 auto;
  background: #ccc;
  list-style: none;

  
}

/* Boxes */
#timeline ul li div {
    margin-bottom: 0;
  position: relative;
  bottom: 0;
  width: 400px;
  padding: 1rem;
  background: var(--secondary-color);
  transition: all 0.5s ease-in-out;
  border: 1px solid #ecececc2;
    background: #ffffff;
    color: white;
    display: inline-block;
    width: 301px;
    padding: 18px;
    border-radius: 4px;
    font-family:sans-serif;

  /* visibility: hidden;
  opacity: 0; */
}
#timeline ul li div h3{margin-bottom:0}

/* Right Side */
#timeline ul li:nth-child(odd) div {
  left: 40px;
  transform: translate(200px, 0);
}

/* Left Side */
#timeline ul li:nth-child(even) div {
  left: -434px;
  transform: translate(-200px, 0);
}

/* Dots */
#timeline ul li:after {
  content: '';
  position: absolute;
  left: 50%;
  bottom: 0;
  width: 25px;
  height: 25px;
  background: inherit;
  border-radius: 50%;
  transform: translateX(-50%);
  transition: background 0.5s ease-in-out;
}

/* Arrows Base */
#timeline div:before {
  content: '';
  position: absolute;
  bottom: 5px;
  width: 0;
  height: 0;
  border-style: solid;
}

#timeline ul li.show:nth-child(odd) div:before{
    border-color: transparent #923233 transparent transparent;
}
#timeline ul li.show:nth-child(even) div:before{
    border-color: transparent transparent transparent #923233;
}

/* Right Side Arrows */
#timeline ul li:nth-child(odd) div:before {
  left: -15px;
  border-width: 8px 16px 8px 0;
  border-color: transparent white transparent transparent;
}

/* Left Side Arrows */
#timeline ul li:nth-child(even) div:before {
  right: -15px;
  border-width: 8px 0px 8px 16px;
  border-color: transparent transparent transparent white;
}

/* Show Boxes */
#timeline ul li.show div {
    transform: none;
    visibility: visible;
    opacity: 1;
    background: #923233;
    border-color: #923233;
    color: white;
}
#timeline ul li.show div h3, #timeline ul li.show div p{color:white;}

#timeline ul li.show:after { background: #923233 }


@media(max-width: 900px) {
  #timeline ul li div { width: 250px; }

  #timeline ul li:nth-child(even) div { left: -284px; }
}

@media(max-width: 600px) {
  #timeline ul li { margin-left: 20px; }

  #timeline ul li div {
    width: calc(100vw - 90px);
  }

  #timeline ul li:nth-child(even) div { left: 40px; }

  #timeline ul li:nth-child(even) div:before {
    left: -15px;
    border-width: 8px 16px 8px 0;
    border-color: transparent var(--secondary-color) transparent transparent;
  }
}
</style>





<header id="main-header">
  <div class="container">
    <h1><i class="fas fa-brain"></i> Knowledge Resume</h1>
    <h3><i class="fas fa-user"></i> Genesis Gabiola</h3>
  </div>
</header>

<section id="timeline">
  <ul>
    <li>
      <div>
        <h3>
        1976
        </h3>
        <p>Formation of VDH Group</p>
      </div>
    </li>

    <li>
      <div>
        <h3>
           1996 - 97
        </h3>
        <p>Started production of Thymol from Menthone</p>
      </div>
    </li>

    <li>
      <div>
        <h3>
           2003
        </h3>
        <p>First in India to start production of thymol from Meta Cresol</p>
      </div>
    </li>

    <li>
      <div>
        <h3>
          2007
        </h3>
        <p>First (and only) manufacturer in India to start production of Meta Cresol (Backward integration) started producing ParaCresol,BHT</p>
      </div>
    </li>

    <li>
      <div>
        <h3>
           2010
        </h3>
        <p>Started new manufacturing facility for production of 100% Natural essential & Spice Oil</p>
      </div>
    </li>

    <li>
      <div>
        <h3>
           2012
        </h3>
        <p>Started Warehousing facility in Europe (Germany)</p>
      </div>
    </li>

    <li>
      <div>
        <h3>
         2014
        </h3>
        <p>New Derivatives from Thymol such as Synthetic Menthones & Synthetic DL-Menthol</p>
      </div>
    </li>

    <li>
      <div>
        <h3>
          2015
        </h3>
        <p>Largest manufacturer of (Butylated Hydroxytoluene) BHT in India. New Derivatives of Meta Cresol & Para Cresol such as 2-tert-butyl-5-methylphenol, 2-tert-butyl-4-methylphenol & Chloro Cresol</p>
      </div>
    </li>

  

  </ul>
</section>
<?php include("include/footer.php"); ?>