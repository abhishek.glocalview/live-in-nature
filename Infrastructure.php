<?php include("include/header.php"); ?>
<div class="wrapp-all listing-page" id="listing">
   <section id="overall-wrp" class="listing-hero">
      <div class=" bk-wrap-white">
         <img src="images/jpeg/infrastructure_&_facillities_banner.png" alt="" class="w-100"/>
      </div>
   </section>
   <section class="over-laping-Div">
      <div class="container bk-wrap-white  wrps-about">
         <div class="padding-mld">
            <div class="about-page-2">
               <h1> Infrastructure & Facilities </h1>
              <p>LIV IN NATURE has high tech manufacturing facilities of extraction using food – grade stainless steel distillers ensuring congruency between the oil chemistry of the plant and its distilled form by making it free from adulterants and impurity, providing 100% pure and natural products.</p> 
               <!-- <div class="wrp-ind">
                   <div class="indus-row">
                       <div class="col-md-6">
                           <div class="row-data-wrap">
                               <div class="data-here">
                                   <h3>REACTION PROCESS</h3>
                               </div>
                               <div class="image-here">
                                   <img src="images/dog-1.png" alt="">
                               </div>
                           </div>
                       </div>

                       <div class="col-md-6">
                           <div class="row-data-wrap">
                               <div class="data-here">
                                   <h3>REACTION PROCESS</h3>
                               </div>
                               <div class="image-here">
                                   <img src="images/dog-1.png" alt="">
                               </div>
                           </div>
                       </div>
                       
                   </div>


                   <div class="indus-row right-paddings">
                       <div class="col-md-6">
                           <div class="row-data-wrap">
                           <div class="image-here">
                                   <img src="images/dog-1.png" alt="">
                               </div>
                               <div class="data-here">
                                   <h3>REACTION PROCESS</h3>
                               </div>
                               
                           </div>
                       </div>

                       <div class="col-md-6">
                           <div class="row-data-wrap">
                               <div class="image-here">
                                   <img src="images/dog-1.png" alt="">
                               </div>
                               <div class="data-here">
                                   <h3>REACTION PROCESS</h3>
                               </div>
                               
                           </div>
                       </div>
                       
                   </div>



               </div> -->
               <img src="images/jpeg/Infrastructure-Facilities.png" alt="" />
                <div class="clearfix"></div>
               <div class="col-md-6 text-left">
                   <br>
               <h2><span class="color-maroon">Our Quality Standards</span></h2>
                  <div class="qouets text-left">
                   
                     <h2>Being a part of this industry since past 40 yrs We don’t need to brag about the quality products we offer as our seed to seal, whether it’s our products or services all of it speaks for itself.</h2>
                    
                  </div>

                  <p>Is our quality standard. The three pillars of this meticulous approach—Sourcing, Science, and Standards—allow us to deliver pure, authentic essential oils and essential oil-infused products that you and your family can use with full peace of mind.</p>
                  <p>We’re committed to doing this by establishing and maintaining the ultimate industry-leading standards for all our  products. For us, the benchmarks for delivering a product that our members can feel great about are multifaceted, robust, and non-negotiable. We’re proud to offer an extensive line of authentic, essential oil-infused solutions that represent the best of nature through the standards of our ground breaking  This unique approach sets us apart. We guide every step of production to ensure the best quality products journey our products take and our unparalleled process </p>

               </div>
               <div class="col-md-6 text-left">
                   
                  <img src="images/jpeg/quality_standards.png" alt="" class="w100" style="margin:13px 0;" />
<p>We’re made up of people—including moms, dads, students, and professionals—who seek lives of wellness and fulfillment. We form a group of like-minded individuals looking to get the most out of life. </p>
<p>Our communities, events, and conventions allow us to come together to enrich and uplift one another in an extraordinary way. Together, we have the power to inspire people around the world.</p>
<p>So, Allow yourself to discover the power of nature & witness the difference it brings to your health and well being.</p>   
                </div>


            <div class="clearfix"></div>
             
             <div class="succs text-left">
             <h2 style="padding-left:30px" ><span class="color-maroon">Certification, Accreditations & Affiliations</span></h2>
             
             <div class="col-md-5">
                <h3>Quality Certificates</h3>
                <ul class="col-Certificates">
                    <li> ISO 9001:2008</li>
                    <li> HACCP</li>
                    <li> WHO-GMP</li>
                    <li> HALAL</li>
                    <li> KOSHER  </li>
                    <li> FSSAI</li>
                    <li> US.FDA</li>
                     
                </ul>
             </div>
             <div class="col-md-7">
                <h3>Our Membership with</h3>
                <ul class="our-members">
                    <li><img src="images/jpeg/fami.png" alt=""></li>
                    <li><img src="images/jpeg/FDA.png" alt=""></li>
                    <li><img src="images/jpeg/FICCI.png" alt=""></li>
                    <li><img src="images/jpeg/fssai.png" alt=""></li>
                    <li><img src="images/jpeg/gmp.png" alt=""></li>
                    <li><img src="images/jpeg/halal-India.png" alt=""></li>
                    <li><img src="images/jpeg/JAS-ANZ.png" alt=""></li>
                    <li><img src="images/jpeg/k-star.png" alt=""></li>
                </ul>
             </div>
             <div class="clearfix"></div>
             </div>

                 
            </div>
         </div>
      </div>
   </section>
   
    
</div>
<br>
<?php include("include/footer.php"); ?>