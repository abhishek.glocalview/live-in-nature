<!--- Our Video end---->
<section class="upper-footer">
   <div class="container text-center">
      <h4 class="text-uppercase" >This is Our Promise to you</h4>
      <p>Lorem ipsum dolor sit amet, vitae fermentum wisi commodo sodales enim ac, nibh turpis, pede curabitur lectus feugiat ac, euismod montes elit sodales turpis felis non. Pretium orci eget consectetuer in, donec et quam. Aute metus, amet </p>
      <div class="client-logos">
      <ul class="footer_client">
                            <li><img class="lazy" data-original="http://www.cinntra.com/assets/cinntra/images/about/oracle.png" alt="" src="images/jpeg/fami.png" style="display: block;"></li>
                            <li><img class="lazy" data-original="http://www.cinntra.com/assets/cinntra/images/about/sap.jpg" alt="" src="images/jpeg/FDA.png" style="display: block;"></li>
                            <li><img class="lazy" data-original="http://www.cinntra.com/assets/cinntra/images/about/ms.png" alt="" src="images/jpeg/FICCI.png" style="display: block;"></li>
                            <li><img class="lazy" data-original="http://www.cinntra.com/assets/cinntra/images/about/14.png" alt="" src="images/jpeg/fssai.png" style="display: block;"></li>
                            <li><img class="lazy" data-original="http://www.cinntra.com/assets/cinntra/images/about/uneecops.png" alt="" src="images/jpeg/gmp.png" style="display: block;"></li>

                            <li><img class="lazy" data-original="http://www.cinntra.com/assets/cinntra/images/about/ms.png" alt="" src="images/jpeg/halal-India.png" style="display: block;"></li>
                            <li><img class="lazy" data-original="http://www.cinntra.com/assets/cinntra/images/about/14.png" alt="" src="images/jpeg/JAS-ANZ.png" style="display: block;"></li>
                            <li><img class="lazy" data-original="http://www.cinntra.com/assets/cinntra/images/about/uneecops.png" alt="" src="images/jpeg/k-star.png" style="display: block;"></li>
<!--                            <li><img class="lazy" data-original="--><!--images/client-logo/c5.png"-->
<!--                                     alt=""></li>-->
                        </ul>

      
      </div>

     
   </div>

</section>

<div class="Available-in">
         <h3>Available On: </h3>
         <div class="afi">
           <a href="http://dev.glocalview.in/livinnature/index.php" target="_blank"><img src="http://dev.glocalview.in/livinnature/image/catalog/logo-liv.png" alt=""  /></a>
         </div>
         <div class="afi">
         <a href="https://amazon.in" target="_blank">
            <img src="images/amazon.png" alt="" />
         </a>
         </div>
         <div class="afi">
            <a href="https://flipkart.com" target="_blank">
            <img src="images/FLIPKART.png" alt="" />
            </a>
         </div>
      </div>
      
      <div class="footer">
         <div class="container">
            <div class="row">
               <div class="col-md-3">
                  <img src="images/logo-liv.png" alt="" style="max-width: 100%;">
               </div>
               <div class="col-md-3 adds">
                  <h6>
                     <Address>Address</Address>
                  </h6>
                  <h5>Indian Spice Oil Industries</h5>
                  <p>AN-34,35,36, Masuri Gulawati Road,
                     UPSIDC Industrial Area Phase – III, Ghaziabad - 201015, Uttar Pradesh, India
                  </p>
                  <p>Phone: +91-9818383839</p>
                  <p>Email: indianspiceoil@gmail.com</p>
               </div>
               <div class="col-md-3 contact-info">
                  <h6>Social Media</h6>
                  <p class="social">
                     <a href="https://www.facebook.com/LivInNature/" target="_blank" class="facebook"> <img src="images/SOCIAL_ICONS/facebook.svg" alt="" /></a>
                      <a href="https://twitter.com/Liv_in_nature" target="_blank" class="facebook"> <img src="images/SOCIAL_ICONS/twitter.svg" alt="" /></a>
                      <a href="#" class="facebook"> <img src="images/SOCIAL_ICONS/google-plus.svg" alt="" /></a>
                      <br>
                      <a href="#" class="facebook"> <img src="images/SOCIAL_ICONS/linkedin.svg" alt="" /></a>

                     <a href="#" class="facebook"> <img src="images/SOCIAL_ICONS/pinterest.svg" alt="" /></a> 
                     <a href="#" class="facebook"> <img src="images/SOCIAL_ICONS/youtube.svg" alt="" /></a>
                     
                  </p>
                
               </div>
               <div class="col-md-3 contact-info">
                  <h6>Subscribe Our newsletter</h6>
                  <form class="subs">
                     <label>
                     <input type="text" placeholder="Enter your email" >
                     <button type="submit"><i class="fa fa-long-arrow-right" aria-hidden="true"></i></button>
                     </label>
                  </form>
               </div>
            </div>
            <div class="row">
               <div class="col-md-12 copyright">
                  <p style="color:white">&copy; 2017. Indian Spice Oil Industries (www.indianspiceoil.com). All rights reserved. <a href="" class="pull-right" style="margin-right:30px;">Design and Development : Glocal View</a></p>

               </div>
            </div>
         </div>
     
      </div>
      <button onclick="topFunction()" id="myBtn" title="Go to top"><i class="fa fa-arrow-up" aria-hidden="true"></i></button>


      <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
      <script data-cfasync="false" src="../../cdn-cgi/scripts/5c5dd728/cloudflare-static/email-decode.min.js"></script>
      <script src="https://code.jquery.com/jquery.js"></script>
      <script src="js/bootstrap.min.js"></script>
      <script src="js/carouFredSel.js"></script>
      <script src="js/jquery.stellar.min.js"></script>
      <script src="js/ekkoLightbox.js"></script>
      <script src="js/custom.js"></script>
      <script>
      $(document).ready(function(){
         $('#nav-icon1,#nav-icon2,#nav-icon3,#nav-icon4').click(function(){
            $(this).toggleClass('open');
            $(".checkshow").slideToggle();
            
         });

         $(".open-tab li").click(function(){
            var data = $(this).attr("id");
            $(".open-tab li").removeClass("active-open-color");
            $(this).addClass("active-open-color");
            $(".taboo1").removeClass("openActive");
            $("."+data).addClass("openActive");
         })
      });
      
      </script>
    <script>
 

 var mybutton = document.getElementById("myBtn");

// When the user scrolls down 20px from the top of the document, show the button
window.onscroll = function() {scrollFunction()};

function scrollFunction() {
  if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
    mybutton.style.display = "block";
  } else {
    mybutton.style.display = "none";
  }
}

// When the user clicks on the button, scroll to the top of the document
function topFunction() {
  document.body.scrollTop = 0;
  document.documentElement.scrollTop = 0;
}

 
$(document).ready(function() {
    "use strict";
	$(".team-list").on("click", "a", function(a) {
        a.preventDefault();
        var e = $(this).data("team");
        $(".team-single").removeClass("active"), $(".team-list li").removeClass("active"), $("#" + e).addClass("active"), $(this).parent().addClass("active")
    });
	
});

</script>
<!--End of Tawk.to Script-->
<!-- Start of  Zendesk Widget script -->
<script id="ze-snippet" src="https://static.zdassets.com/ekr/snippet.js?key=84c084f2-36f8-41ad-ba7c-f03e084643d0"> </script>
<!-- End of  Zendesk Widget script -->



<script>
const items = document.querySelectorAll('#timeline li');

const isInViewport = el => {
  const rect = el.getBoundingClientRect();
  return (
    rect.top >= 0 &&
    rect.left >= 0 &&
    rect.bottom <=
      (window.innerHeight || document.documentElement.clientHeight) &&
    rect.right <= (window.innerWidth || document.documentElement.clientWidth)
  );
};

const run = () =>
  items.forEach(item => {
    if (isInViewport(item)) {
      item.classList.add('show');
    }
  });

// Events
window.addEventListener('load', run);
window.addEventListener('resize', run);
window.addEventListener('scroll', run);
 
</script>
   </body>
</html>