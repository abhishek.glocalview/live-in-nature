<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <title>Live in Nature</title>
      <link rel="icon" href="images/logo-liv.png" type="image/gif" sizes="16x16">
      <link href="css/bootstrap.min.css" rel="stylesheet">
      <link href="css/style.css?v1.2" rel="stylesheet">
      <link href="css/style-2.css?v1.2" rel="stylesheet" />
      <link href="https://fonts.googleapis.com/css?family=Cabin:400,500,600,700,400italic,500italic,600italic,700italic" rel='stylesheet' type='text/css'>
      <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700,900,400italic,700italic,900italic" rel='stylesheet' type='text/css'>
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
      <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
      <![endif]-->
      <!--[if IE 8]>
      <link rel="stylesheet" type="text/css" href="css/ie8.css" />
      <![endif]-->
   </head>
   <body class="homepage">
      <div class="navbar navbar-default navbar-fixed-top" role="navigation">
         <div class="container">
            <div class="navbar-header">
               <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
               <span class="sr-only">Toggle navigation</span>
               <span class="icon-bar"></span>
               <span class="icon-bar"></span>
               <span class="icon-bar"></span>
               </button>
               <h1><a class="navbar-brand" href="index.php">
                  <img src="images/logo-liv.png" alt="" style=" width: 95px;margin-top: -7px;padding-bottom: 8px;" />
                  </a>
               </h1>
            </div>
            <div class="navbar-collapse collapse">
               <ul class="nav navbar-nav">
                  <li class="active checkshow">
                     <a href="index.php" title="Home"><span data-hover="Home">Home</span></a>
                        
                  </li>
                  <li class="checkshow">
                     <a href="javascript:;" title="Our Story"><span data-hover="Our Story">Our Story </span></a>
                     <div class="mega-menu">
                           <div class="display-flexs">
                           <ul> 
                               <li><a href="about.php">About Us</a></li>
                              <li><a href="Infrastructure.php">Infrastructure</a></li>
                              <li><a href="crs.php">Our CSR Activity</a></li> 
                              <li><a href="caution.php">Caution</a></li>
                           </ul>
                           
                           </div>
                        </div>
                  </li>
                  <li class="checkshow">
                     <a href="listing.php" title="Products"><span data-hover="Products">Products</span></a>

                     <div class="mega-menu">
                           <div class="display-flexs">
                           <ul>
                              <li><a href="#" class="nav-title">spice oil</a></li>
                             
                      <li><a  href="#">Ajwain Oil</a></li>

                      <li><a href="#">Black Pepper Oil</a></li>

                      <li><a href="#">Cardamom Oil</a></li>

                      <li><a href="#">Carrot Seed Oil</a></li>

                      <li><a href="#">Celery Seed Oil</a></li>

                      <li><a href="#">Cuminic Seed Oil</a></li>

                      <li><a   href="#">Dill seed Oil</a></li>

                      <li><a href="#">Fennel Seed Oil</a></li>

                      <li><a href="#">Nutmeg Oil</a></li>

                              
                           </ul>
                           <ul>
                              <li><a href="#" class="nav-title" >Essential Oils</a></li>
                    

                                 <li><a href="#">Basil Oil</a></li>

                                 <li><a href="#">Citronella Oil</a></li>

                                 <li><a href="#">Cypriol Oil</a></li>

                                 <li><a  href="#">Juniper Berry Oil</a></li>

                                 <li><a href="#">Lemon Grass OIl</a></li>

                                 <li><a href="#">Palmarosa Oil</a></li>

                                 <li><a  href="#">Peppermint Oil</a></li>

                                 <li><a href="#">Spearmint Oil</a></li>

                                 <li><a href="#">Thyme Oil (Red)</a></li>

                                 <li><a href="#">Jamrosa Oil</a></li>

                                 <li><a  href="#">Eucalyptus Oil</a></li>

                                 <li><a href="#">Clove Oil</a></li>

                             

                           </ul>
                           <ul>
                           <li><a href="upcoming.php" class="nav-title">Upcoming Products</a></li>
                           
                           <li><a   href="upcoming.php">Calamus Root Oil</a></li>

                         <li><a href="upcoming.php">Kapoor Kachri Oil</a></li>

                        <li><a href="upcoming.php">Parsley Seed Oil</a></li>

                        <li><a href="upcoming.php">Sugandh Mantri Oil</a></li>

                           </ul>
                           </div>
                        </div>

                  </li>
                 
                  
                  <li class="checkshow">
                     <a href="https://dev.glocalview.in/livinnature/index.php?route=blog/blog" target="_blank" title="Blogs"><span data-hover="Blogs">Blogs </span></a>
                  </li>

                  <li class="checkshow">
                     <a href="contact.php"  title="Contact Us"><span data-hover="Contact Us">Contact Us </span></a>
                  </li>
                   
                 <li>
                   
                       
                           <div id="nav-icon4" class="open">
                              <span></span>
                              <span></span>
                              <span></span>
                           </div>
                    
                 </li>
                 
                  <li class="purchase-btn">
                     <form action="http://dev.glocalview.in/livinnature/index.php?route=common/home" target="_blank" >
                        <button type="submit" class="btn btn-default btun">  Shop Now</button>
                     </form>
                  </li>
               </ul>
            </div>
         </div>
      </div>