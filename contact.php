<?php include("include/header.php"); ?>
<div class="wrapp-all listing-page" id="listing">
   <section id="overall-wrp" class="listing-hero">
      <div class=" bk-wrap-white">
         <img src="images/jpeg/contact_us_banner.png" alt="" class="w-100"/>
      </div>
   </section>
   <section class="over-laping-Div">
      <div class="container bk-wrap-white  wrps-about">
         <div class="padding-mld">
            <div class="about-page-2 text-left">
               
               
               <!-- forms start-->

<div class="row">
<div class="col-md-5">
    <h1>Indian Spice Oil Industries </h1>
    <br>
<ul class="contact-infoo">
<li class="address">
 <span>Address : </span>  AN-34,35,36, Masuri Gulawati Road, UPSIDC Industrial Area Phase – III
Ghaziabad - 201015, Uttar Pradesh, India </li>
<li class="telephone">
<span><i class="fa fa-phone" aria-hidden="true"></i>   </span>   +91-9818383839, 9818427779
</li>

<li class="mail">
<a href="mailto:indianspiceoil@gmail.com"> <span><i class="fa fa-envelope" aria-hidden="true"></i>   </span> info@indianspiceoil.com
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;gagandeep@indianspiceoil.com</a>
</li>

<li class="wev">
<a href="www.liveinnature.com"> <span> <i class="fa fa-paper-plane" aria-hidden="true"></i>   </span> www.liveinnature.com</a>
</li>
</ul>
</div>

<div class="col-md-6 col-md-offset-1">
    <h2><strong>Below Type Here</strong></h2>
<form role="form" id="contact_form">
<div class="form-group">
<label for="InputName">Your Name</label>
<input type="text" class="form-control" id="InputName" placeholder="Your name">
</div>
<div class="form-group">
<label for="InputEmail">Your email</label>
<input type="email" class="form-control" id="InputEmail" placeholder="Your email">
</div>
<div class="form-group">
<label for="InputEmail">Phone number</label>
<input type="text" class="form-control"  placeholder="Phone number">
</div>
<div class="form-group">
<label for="InputEmail">Subject</label>
<input type="email" class="form-control" id="InputEmail" placeholder="Subject">
</div>
<div class="form-group">
<label for="InputMesaagel">Your messsage</label>
<textarea class="form-control" id="Message" placeholder="Your message" rows="4"></textarea>
</div>
<button type="submit" class="btn btn-default btn-green">Send message</button>
</form>
</div>

</div>

               <!--- forms end---->
                
              
            </div>
         </div>
      </div>
   </section>
  
</div>
<?php include("include/footer.php"); ?>