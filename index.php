<?php include("include/header.php"); ?>
      <div class="hero-wrap">            
      <img src="images/bk.png" alt="" class="w-100" style="margin-bottom: 50px" />
      <div class="hero-overlay">
         <div class="container">
            <div class="overlay-middle">    
            <h2>Welcome To " INDIAN spice oil industries " </h2>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua</p>
            </div>

            <div class="plays">
               <h3 class="pull-left"><i class="fa fa-play-circle" aria-hidden="true"></i> Watch Video</h3>
               <h3 class="pull-right"><i class="fa fa-volume-off" aria-hidden="true"></i> OFF</h3>
            </div>
         </div>
         </div>
      </div>
      <section id="overall-wrp">
         <div class="container bk-wrap-white">
            <img src="images/bk2.jpg" alt="" class="w-100"/>
            <div class="container-fluid Bottom-imgs klikk">
               <div class="wrap-reltv-box">
                  
                  <div class="about-wrp-oli bk-color first-bk-img">
                  <ul class="open-tab">
                     <li id="open1" class="active-open-color"><h1>What is <span class="color-maroon">Spice Oil ?</span></h1></li>
                     <li id="open2"><h1>What is <span class="color-maroon">Essential Oils ?</span></h1></li>
                  </ul>
                  <div class="taboo1 open1 openActive">
                     
                     <p class="wrap-para">Spice oil is a spice derivative that are extracted generally by steam distillation process.These oils are the volatile components present in spices and provide the aroma and flavor of the spice they are made from. India is one of the top most producers of spice oils and contributes to around 70% of the total spice oil production. Countries including USA, EU, Japan and other middle eastern countries are major importers of spice oil. The exports of Indian spice oils has been rising significantly in the last few years owing to a sharp rise of demand in the food sector. </p>
                     <a href="#Advantagespice" class="btun btn-color">Know More</a>
                  </div>
                  <div class="taboo1 open2">
                     
                     <p class="wrap-para">An essential oil is a concentrated hydrophobic liquid containing volatile aroma compounds from plants. Essential oils are also known as volatile oils, ethereal oils, aetherolea, or simply as the oil of the plant from which they were extracted.An oil is "essential" in the sense that it contains the "essence of" the plant's fragrance—the characteristic fragrance of the plant from which it is derived.They are used in perfumes, cosmetics, soaps and other products, for flavoring food and drink, and for adding scents to incense and household cleaning products..</p>
                     <a href="#EssentialOil" class="btun btn-color">Know More</a>
                  </div>
                  </div>
                  <div class="about-wrp-oli bk-color">
                     <h2><span class="color-maroon" >Popular</span> Spice Oils</h2>
                     <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. <br>consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                     <div class="wrap-spice">
                        <div class="col-md-2">
                           <img src="images/p_1.jpg" alt="" class="w-100" />
                           <h5>Mint Oil</h5>
                        </div>
                        <div class="col-md-2">
                           <img src="images/p_2.jpg" alt="" class="w-100" />
                           <h5>Cardamom Oil</h5>
                        </div>
                        <div class="col-md-2">
                           <img src="images/p_3.jpg" alt="" class="w-100" />
                           <h5>Ginger Oil</h5>
                        </div>
                        <div class="col-md-2">
                           <img src="images/p_4.jpg" alt="" class="w-100" />
                           <h5>Pepper Oil</h5>
                        </div>
                        <div class="col-md-2">
                           <img src="images/p_5.jpg" alt="" class="w-100" />
                           <h5>Cinnamon Oil</h5>
                        </div>
                        <div class="col-md-2">
                           <img src="images/p_6.jpg" alt="" class="w-100" />
                           <h5>Clove Oil</h5>
                        </div>
                     </div>
                     <div class="clearfix"></div>
                     <br><br>
                     <a href="http://dev.glocalview.in/livinnature/index.php?route=common/home" target="_blank" class="btun" >Shop Now</a>
                  </div>
                  <div class="about-wrp-oli bk-color" id="Advantagespice">
                     <h2><span class="color-maroon">Advantage</span> of Spice Oils</h2>
                     <p><span class="color-maroon">Hygiene : </span>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. <br>consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                     <p><span class="color-maroon">Standardizeating : </span>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. <br>consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                     <p><span class="color-maroon">Consistency : </span>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. <br>consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                     <div class="wrap-spice top-three-sp">
                        <div class="col-md-3 col-md-offset-1">
                           <img src="images/a_1.jpg" alt="" class="w-100" />
                           <h4>Hygiene </h4>
                        </div>
                        <div class="col-md-3">
                           <img src="images/a_2.jpg" alt="" class="w-100" />
                           <h4>Standardizeating  </h4>
                        </div>
                        <div class="col-md-4">
                           <img src="images/a_3.jpg" alt="" class="w-100" style="margin-top: 23px;" />
                           <h4>Consistency  </h4>
                        </div>
                     </div>
                     <div class="clearfix"></div>
                  </div>
                  <div class="about-wrp-oli bk-color">
                     <h2> <span class="color-maroon"> Use of</span> Spice Oil</h2>
                     <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. <br>consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                     <div class="wrap-spice">
                        <div class="col-md-2 col-md-offset-1">
                           <img src="images/u_1.jpg" alt="" class="w-100" />
                           <h5>Personal Hygiene</h5>
                        </div>
                        <div class="col-md-2">
                           <img src="images/u_2.jpg" alt="" class="w-100" />
                           <h5>Aromatherapy</h5>
                        </div>
                        <div class="col-md-2">
                           <img src="images/u_3.jpg" alt="" class="w-100" />
                           <h5>Pharmaceuticals</h5>
                        </div>
                        <div class="col-md-2">
                           <img src="images/u_4.jpg" alt="" class="w-100" />
                           <h5>Beverages</h5>
                        </div>
                        <div class="col-md-2">
                           <img src="images/u_5.jpg" alt="" class="w-100" />
                           <h5>Food Processing</h5>
                        </div>
                      
                     </div>
                     <div class="clearfix"></div>
                     <br><br>
                     <a href="http://dev.glocalview.in/livinnature/index.php?route=common/home" target="_blank" class="btun" >Shop Now</a>
                  </div>
               </div>
            </div>
         </div>
      </section>
      <!--- Our Video start---->
      <section class="Featured_Products_wrap">
         <div class="container bk-wrap-white first-bk-img Featured_Products">
            <div class="Bottom-imgs padding-bottom-40">
         <div class="padding-mld">
         <span class="btun btn-color sub-btn">Featured Products</span>
         <h2 class="title-holder"><span>Our <strong class="color-maroon">Featured</strong> Products</span></h2>
         <div class='row'>
            <div class='col-md-12' id="index-fe">
               <div class="carousel slide " data-ride="carousel" id="quote-carousel">
                  <!-- Bottom Carousel Indicators -->
                  <ol class="carousel-indicators">
                     <li data-target="#quote-carousel" data-slide-to="0" class="active"></li>
                     <li data-target="#quote-carousel" data-slide-to="1"></li>
                     <li data-target="#quote-carousel" data-slide-to="2"></li>
                  </ol>
                  <!-- Carousel Slides / Quotes -->
                  <div class="carousel-inner">
                     <!-- Quote 1 -->
                     <div class="item active">
                        <div class="row">
                           <div class="col-sm-7 text-left">
                              <small><strong>Rosemary</strong> Oil</small>
                              <h4><strong class="color-maroon">MAIN COMPONENTS : </strong> Lorem Ipsum ist ein einfacher Demo-Text für die Print</h4>
                              <p>Lorem Ipsum ist ein einfacher Demo-Text für die Print- und Schriftindustrie. Lorem Ipsum ist in der Industrie bereits der Standard Demo-Text .</p>
                           </div>
                           <div class="col-sm-5 text-center">
                              <img class="img-responsive" src="images/1.jpg" >
                           </div>
                        </div>
                     </div>
                     <div class="item">
                        <div class="row">
                        <div class="col-sm-7 text-left">
                              <small><strong>Rosemary</strong> Oil</small>
                              <h4><strong class="color-maroon">MAIN COMPONENTS : </strong> Lorem Ipsum ist ein einfacher Demo-Text für die Print</h4>
                              <p>Lorem Ipsum ist ein einfacher Demo-Text für die Print- und Schriftindustrie. Lorem Ipsum ist in der Industrie bereits der Standard Demo-Text .</p>
                           </div>
                           <div class="col-sm-5 text-center">
                              <img class="img-responsive" src="images/1.jpg" >
                           </div>
                        </div>
                     </div>
                  </div>
                  <!-- Carousel Buttons Next/Prev -->
                  <a data-slide="prev" href="#quote-carousel" class="left carousel-control"><i class="fa fa-long-arrow-left"></i></a>
                  <a data-slide="next" href="#quote-carousel" class="right carousel-control"><i class="fa fa-long-arrow-right"></i></a>
               </div>
               <a href="listing.php" class="view-all-web">View All &nbsp; <i class="fa fa-long-arrow-right" aria-hidden="true"></i></a>
            </div>
         </div>
         </div>
      </section>
      <!--- Our Video end---->
      <!--- Our Video start---->
      <section class="Featured_Products_wrap" id="video">
         
         <div class="container bk-wrap-white first-bk-img Featured_Products">
            <div class="Bottom-imgs">
            <div class="padding-mld">
               <h2 class="title-holder"><span>Our <strong class="color-maroon">Video</strong></span></h2>
               <div class="wrap-spice">
                  <div class="col-md-12">
                     <img src="images/video-img.png" alt="" />
                  </div>
               </div>
               <div class="clearfix"></div>
               <h4 class="sub-sub-title">
                  
               </h4>
            </div>
         </div>
         </div>
      </section>
      <!--- Our Video end---->
      <!--- Our Video start---->
      <section class="our_process" id="EssentialOil">
         <div class="container bk-wrap-white Bottom-imgs first-bk-img">
            <div class="Bottom-imgs ">
            <div class="padding-mld">
               <div class="">
                  <h2 class="title-holder">Our <span class="color-maroon">Process</span></h2>
                  <div class="wrap-spice">
                     <img src="images/process.png" alt=""  />
                  </div>
                  <h3 class="sub-sub-title">Steam Distillation Process</h3>
               </div>
            </div>
         </div>
         </div>
      </section>
      <!--- Our Video end---->
      <!--- Our Video start---->
      <section class="testimonial-section">
         <div class="container bk-wrap-white Bottom-imgs">
            <div class="padding-mld">
               <div class="">
                  <span class="btun btn-color sub-btn">Our Customer</span>
                  <h2 class="title-holder"><span>Our <strong class="color-maroon" >Customer</strong> - What They Say</span></h2>
                  <div class="seprator"></div>
                  <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                     <div class="controls">
                        <a class="left fa fa-long-arrow-left btn btn-default testimonial_btn" href="#carousel-example-generic"
                           data-slide="prev"></a>
                        <a class="right fa fa-long-arrow-right btn btn-default testimonial_btn" href="#carousel-example-generic"
                           data-slide="next"></a>
                     </div>
                     <!-- Wrapper for slides -->
                     <div class="carousel-inner">
                        <div class="item active">
                           <div class="col-sm-12 text-center" >
                              <img src="http://demos1.showcasedemos.in/jntuicem2017/html/v1/assets/images/jack.jpg" class="img-responsive" style="width: 80px ; display: inline-block;">
                           </div>
                           <div class="col-sm-12">
                              <p class="testimonial_subtitle">
                                 <span>Praveen Garg</span><br>
                                 <!-- <span>Flipkart Customer</span> -->
                              </p>
                           </div>
                           <div class="row" style="padding: 20px">
                              <p class="testimonial_para">   The Liv With Nature products are wonderful. Being able to understand every ingredient that your using on your body is so important. Outside of that the products do what they are supposed too and th..</p>
                              <br>
                              <div class="row">
                              </div>
                           </div>
                        </div>
                        <div class="item ">
                           <div class="col-sm-12 text-center" >
                              <img src="http://demos1.showcasedemos.in/jntuicem2017/html/v1/assets/images/kiara.jpg" class="img-responsive" style="width: 80px ; display: inline-block;">
                           </div>
                           <div class="col-sm-12">
                              <p class="testimonial_subtitle">
                                 <span>Meenakshi</span><br>
                                 <!-- <span>Flipkart Customer</span> -->
                              </p>
                           </div>
                           <div class="row" style="padding: 20px">
                              <p class="testimonial_para">   This household cleaner is literally the truth! I love the way my home smells after use and I am not choking for air when trying to breathe. Will definitely be buying this product again  ..</p>
                              <br>
                              <div class="row">
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
                  <a href="#" class="view-all-web">View All &nbsp; <i class="fa fa-long-arrow-right" aria-hidden="true"></i></a>
               </div>
            </div>
         </div>
      </section>
      <?php include("include/footer.php"); ?>