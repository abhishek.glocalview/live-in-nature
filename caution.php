<?php include("include/header.php"); ?>
<div class="wrapp-all listing-page" id="listing">
   <section id="overall-wrp" class="listing-hero">
      <div class=" bk-wrap-white">
         <img src="images/jpeg/caution_banner.png" alt="" class="w-100"/>
      </div>
   </section>
   <section class="over-laping-Div">
      <div class="container bk-wrap-white  wrps-about">
         <div class="padding-mld">
            <div class="about-page-2 text-left">
               <h1><img src="images/jpeg/caution.svg" alt="" style="vertical-align: text-bottom;"> Caution</h1>
                
               <div class="col-md-6 text-left">
                  <p><span class="color-maroon"><strong>Disclaimer :</strong></span> For external use only. Never apply essential oils directly over the skin.  Always add few drops of essential oil to a tbsp. of base oil like –argon, coconut, almond or olive oil while applying on the skin. Never put essential oils in your eyes, nose & ears. (Avoid the use in case of sensitive skin & skin patch test is always advisable)</p>
                  <div class="qouets text-left">
                      <h3 class="patch-title" ><span class="color-maroon">Patch</span> test</h3>
                     <h2>  A patch test allows you to see how your skin reacts to particular oils before you perform a full application.</h2>
                    
                  </div>

                  <h4>Here are few simple steps for conducting a patch test: </h4>

                  <ul class="counts-j">
                      <li><span>01</span>  Wash your forearm with unscented soap.</li>
                      <li><span>02</span> Pat dry.</li>
                      <li><span>03</span> Rub a few drops of diluted essential oil into a small patch of your  forearm</li>
                      <li><span>04</span>  Wait 24 hours.</li>
                      <li><span>05</span> Remove the gauze.</li>
                  </ul>

                <p>If the skin patch is red, itchy, blistering, or swollen, you have had an adverse reaction to the oil and should discontinue use.</p>
                <p>If you experience discomfort before the 24-hour period ends, immediately wash the area with soap and warm water.</p>

                  <p><span class="color-maroon"><strong>More Precautions :</strong></span> Also, like with all essential oils, consult a medical practitioner before using especially in case you are pregnant, breastfeeding or taking any special medication. Avoid sunlight or UV rays for up to 12hrs after applying essential oils. </p>
                  <p>Store in a cool & dry place and keep away from any inflammatory objects.</p>


               </div>
               <div class="col-md-6 text-right">
                   
                  <img src="images/jpeg/caution_image.png" alt="" class="w100" />
               </div>
                 
            </div>
         </div>
      </div>
   </section>
   
</div>
<br>
<?php include("include/footer.php"); ?>